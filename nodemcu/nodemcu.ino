#include <ESP8266WiFi.h>
#include <WebSocketsServer.h>
#include <Adafruit_BMP280.h>
#include <QMC5883LCompass.h>

// Wi-Fi erişim noktası bilgileri
const char *ssid = "a";
const char *password = "12345678";


IPAddress ip(192, 168, 1, 1);  



#define BMP_SCK 13
#define BMP_MISO 12
#define BMP_MOSI 11
#define BMP_CS 10

Adafruit_BMP280 bmp;  // I2C
QMC5883LCompass compass;


const int tank_pin_back = 14;
const int tank_pin_front_a = A0;

const int motor1 = 16;
const int motor2 = 10;
const int motor3 = 2;
const int motor4 = 0;
const int motor5 = 12; // itis motoru
const int motor6_1 = 13;
const int motor6_2 = 15; 






//Adafruit_BMP280 bmp;  // I2C
float sicaklik = 0, basinc = 0, irtifa = 0;
int heading;
boolean itis_motoru_aktif = false;

long x_value = 0, y_value = 0;  // denizalti yatay eksen konum bilgileri

unsigned long itis_motoru_calisma_zamani = 0;




enum Tank {EMPTY, HALF,  FULL};
enum Tank_motor { IN, IDLE, OUT,};
Tank tank_status;
Tank_motor tank_motor = IDLE;


// WebSocket sunucusu
WebSocketsServer webSocket = WebSocketsServer(81);

// LED durumu
bool ledState = false;

// HTML sayfası (buton içerir)
const char *htmlPage = R"rawliteral(
<!DOCTYPE html>
<html>
<head>
  <title>LED Kontrol</title>
</head>
<body>
  <h1>LED Kontrol</h1>
  <button onclick="toggleLED()">LED'i Aç/Kapat</button>
  <script>
    // var Socket;
    const ip = window.location.hostname;
    let message = "";
    
    function init() {
      Socket = new WebSocket('ws://' + ip + ':81/');
      Socket.onmessage = function(event) {
        if (event.data == "|") {
          //document.getElementById("rxConsole").value = message;
          console.log(message);
         /*           
          document.getElementById("sicaklik").textContent = message.split(",")[0] + " °C";
          document.getElementById("basinc").textContent = message.split(",")[1] + " pa";
          document.getElementById("irtifa").textContent = message.split(",")[2] + " m";
          document.getElementById("heading").textContent = message.split(",")[3] + " °";
        */        
          message = "";
                    
        }
        else {
          message += event.data;
        }
      }
    }




/*


    const socket = new WebSocket('ws://' + location.host + ':81/');
    */
    function toggleLED() {
      socket.send('toggle');
    }
  </script>
</body>
</html>
)rawliteral";

// HTTP sunucusu işlevi
WiFiServer httpServer(80);







// return balance tank situation
Tank get_tank_status() {
  if (digitalRead(tank_pin_back) == LOW)  // empty
    return EMPTY;
 // else if (analogRead(tank_pin_front) < 50)  // full
  else if (analogRead(tank_pin_front_a) > 500)
    return FULL;
  return HALF;
}







// WebSocket istemci mesaj işleyici
void webSocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t length) {
  if (type == WStype_TEXT) {
    String message = (char *)payload;
    
    Serial.println("mesaj: " + message);
   
    if (message == "toggle") {
      
      ledState = !ledState;
      //digitalWrite(2, ledState ? LOW : HIGH); // LOW: LED AÇIK, HIGH: LED KAPALI
      webSocket.sendTXT(num, ledState ? "LED Açıldı" : "LED Kapandı");

    }

    // dibe dal
    if (message == "1") {

      tank_status = get_tank_status();
      if (tank_status != FULL) {  // tank is not full, you can draw water
        digitalWrite(motor6_1, HIGH);
        digitalWrite(motor6_2, LOW);
        tank_motor = IN;
      }
    }
    // yüzeye çık
    else if (message == "2") {
      tank_status = get_tank_status();
      if (tank_status != EMPTY) {  // tank is not full, you can draw water
        digitalWrite(motor6_1, LOW);
        digitalWrite(motor6_2, HIGH);
        tank_motor = OUT;
      }
    }
    // dikey harekti durdur
    else if (message == "3") {
      digitalWrite(motor6_1, LOW);
      digitalWrite(motor6_2, LOW);
      tank_motor = IDLE;

    }
    // sicaklik ve basınc, yon bilgilerini  esp ye gonder
    else if (message == "4") {
      compass.read();
      heading = compass.getAzimuth();
      sicaklik = bmp.readTemperature();
      basinc = bmp.readPressure();
      irtifa = bmp.readAltitude(1013.25);

      String data = String(sicaklik) + "," + String(basinc) + "," + String(irtifa) + "," + String(heading) + "|";

      webSocket.sendTXT(num, data);
      Serial.println(data);
    }
    // ilerle
    else if (message == "5") {
      digitalWrite(motor5, HIGH);
      itis_motoru_aktif = true;
      itis_motoru_calisma_zamani = millis();
      webSocket.sendTXT(num, "motor ilerliyor");
    }
    // ilerlemeyi durdur
    else if (message == "6") {
      digitalWrite(motor5, LOW);
      itis_motoru_aktif = false;
      webSocket.sendTXT(num, "ilerleme durduruldu");
    }
    // sola don
    else if (message == "7") {
      digitalWrite(motor1, LOW);
      digitalWrite(motor2, HIGH);
      digitalWrite(motor3, LOW);
      digitalWrite(motor4, HIGH);
    }
    //sağa don
    else if (message == "8") {
      digitalWrite(motor1, HIGH);
      digitalWrite(motor2, LOW);
      digitalWrite(motor3, HIGH);
      digitalWrite(motor4, LOW);
    }
    // yan yan sağa git
    else if (message == "9") {
      digitalWrite(motor1, HIGH);
      digitalWrite(motor2, HIGH);
      digitalWrite(motor3, LOW);
      digitalWrite(motor4, LOW);
    }
    // yan yan sola git
    else if (message == "10") {
      digitalWrite(motor1, LOW);
      digitalWrite(motor2, LOW);
      digitalWrite(motor3, HIGH);
      digitalWrite(motor4, HIGH);
    }
    // yon motorlari stop
    else if (message == "11") {
      digitalWrite(motor1, LOW);
      digitalWrite(motor2, LOW);
      digitalWrite(motor3, LOW);
      digitalWrite(motor4, LOW);
    }

  }
}


// gemiyi rota açısına yonelmiş hale getir
// ve zaman boyunca ilerle
void rotaya_yonel(int rota) {
  long zaman = sqrt(x_value * x_value + y_value * y_value);
  compass.read();
  heading = compass.getAzimuth();

/*
  while (abs(rota - heading) < 10) {
    motor1.run(FORWARD);
  }*/
}


// baslangic noktasina donmek icin gerekli yon vektor acısını belirle
int rota_belirle() {
  int rota;
  int egim_acisi = atan(y_value / x_value);

  if (y_value > 0 && x_value > 0) {  // 1.bolge
    rota = -(90 + egim_acisi);
  } else if (y_value > 0 && x_value < 0) {  // 2.bolge
    rota = 90 - egim_acisi;
  } else if (y_value < 0 && x_value < 0) {  // 3.bolge
    rota = 90 - egim_acisi;
  } else if (y_value < 0 && x_value > 0) {  // 4.bolge
    rota = -(90 + egim_acisi);
  }

  return rota;
}




void setup() {
  // Seri portu başlat
  Serial.begin(115200);
  delay(1000);


  pinMode(motor1, OUTPUT);
  pinMode(motor2, OUTPUT);
  pinMode(motor3, OUTPUT);
  pinMode(motor4, OUTPUT);
  pinMode(motor5, OUTPUT);
  pinMode(motor6_1, OUTPUT);
  pinMode(motor6_2, OUTPUT);


   
  digitalWrite(motor1, LOW);
  digitalWrite(motor2, LOW);
  digitalWrite(motor3, HIGH);
  digitalWrite(motor4, LOW);
  digitalWrite(motor5, LOW);
  digitalWrite(motor6_1, LOW);
  digitalWrite(motor6_2, LOW);


/*
  // basınc sensoru kontrol
  while (!bmp.begin()) {
    Serial.println(F("BMP280 sensörü bulunamadı, bağlantıyı kontrol edin!"));
    delay(1000);
  }
*/

  // QMC5883L
  compass.setCalibrationOffsets(463.00, 1.00, -148.00);
  compass.setCalibrationScales(1.13, 1.00, 0.89);
  compass.init();


  pinMode(tank_pin_back, INPUT_PULLUP); 

  // Wi-Fi erişim noktasını başlat
  WiFi.softAP(ssid, password);
  WiFi.softAPConfig(ip, ip, IPAddress(255, 255, 255, 0));
  Serial.println("Erişim noktası başlatıldı");
  Serial.print("IP adresi: ");
  Serial.println(WiFi.softAPIP());

  // HTTP sunucusunu başlat
  httpServer.begin();
  Serial.println("HTTP sunucusu başlatıldı");

  // WebSocket sunucusunu başlat
  webSocket.begin();
  webSocket.onEvent(webSocketEvent);
  Serial.println("WebSocket sunucusu başlatıldı");
  digitalWrite(motor5, HIGH);
}

void loop() {
  
  // WebSocket mesajlarını kontrol et
  webSocket.loop();

  // HTTP istemci isteklerini kontrol et
  WiFiClient client = httpServer.available();
  if (client) {
    if (client.connected()) {
      client.print("HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n");
      client.print(htmlPage);
    }
    delay(1000);
    client.stop();
  }

  
  // itis motoru 0.5 saniyedir aktif ise
  if (itis_motoru_aktif && millis() - itis_motoru_calisma_zamani > 500) {
    itis_motoru_calisma_zamani = millis();
    compass.read();
    heading = compass.getAzimuth();

    x_value += 500 * sin(heading * PI / 180);
    y_value += 500 * cos(heading * PI / 180);
  }

/*
  int analogValue = analogRead(A0); 
  Serial.println(analogValue); // Okunan değeri seri porttan yazdır 
  */
 

}
