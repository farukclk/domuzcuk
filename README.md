# Mini Submarine Project (Codename: Domuzcuk)

- NodeMCU
- L293D motor driver
- QMC5883L 3-Axis Compass Sensor
- BMP280 Pressure Sensor
- 6 V 250 RPM DC Motor 
- 18650 3.7 V Li-ion Battery (x2)
- TP4056H 5V 1A Lithium Battery Charger (x2)
- Micro Peristaltic Pump DC 6V
- Syringe


<br>




## Project Photos
You can find photos related to the project [here](https://drive.proton.me/urls/GDP82GK6CM#IP1tz6HB6geg).


## Support Development

  [![Buy Me A Coffee](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/farukclk)


<br>
